// Erle Eikman  8/24/2018
// CanCtrlPanel.h

#ifndef CAN_CTRL_PANEL_H
#define CAN_CTRL_PANEL_H

#include "MCP2515.h"

#define CMD_BUFFER 64


class CanCtrlPanel;
void printHello(CanCtrlPanel *canCtrlPanel);
void reset(CanCtrlPanel *canCtrlPanel);
void regDump(CanCtrlPanel *canCtrlPanel);
void help(CanCtrlPanel *canCtrlPanel);

typedef void (*PanelFunc)(CanCtrlPanel *canCtrlPanel);

typedef struct
{
    char *cmdStr;
    char *description;
    PanelFunc function;
} cmdArray;

static const cmdArray commands[] =
{
   {"hello", "Print hello", &printHello},
   {"dump",  "Print all register values", &regDump},
   {"reset", "Reset MCP2515", &reset},
   {"help",  "List commands", &help}
};
#define COMMANDS (sizeof(commands)/sizeof(commands[0]))

class CanCtrlPanel : public MCP2515
{
public:
    CanCtrlPanel(void);
    unsigned char run(void);

private:
    void command(void);
    void executeCmd(void);
    void nextField(void);

    Mcp2515SpiIoMsg msg;
    char cmd[CMD_BUFFER];         // a String to hold incoming data
    int current;
    int currentField;
    int lastField;

    // We're all friends here.
    friend void printHello(CanCtrlPanel *canCtrlPanel);
    friend void printReg(CanCtrlPanel *canCtrlPanel, unsigned char addr);
    friend void regDump(CanCtrlPanel *canCtrlPanel);
    friend void reset(CanCtrlPanel *canCtrlPanel);
    friend void help(CanCtrlPanel *canCtrlPanel);
};

#endif
