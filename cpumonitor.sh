#!/bin/bash

COUNT=1

while [ $COUNT -gt 0 ]; do

    cpu=$(</sys/class/thermal/thermal_zone0/temp)
    cansend can0 074#`printf "%08x" $cpu`
    sleep 1

done

