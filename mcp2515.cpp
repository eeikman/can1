// Erle Eikman  8/24/2018
#include <Arduino.h>
#include <SPI.h>
#include "MCP2515.h"

void MCP2515::begin(int addr0, int addr1, int spiClk, int cs)
{
    _addr0 = addr0;
    _addr1 = addr1;
    _cs = cs;
    _spiClk = spiClk;

    ch0Data = false;
    ch1Data = false;

    SPI.begin();

    pinMode(_cs, OUTPUT);
    digitalWrite(_cs, HIGH);

    // Reset for development puposes
    msg.cmd = MCP2515_RESET;
    spiIo();
    delay(10);

    // MCP2515 initialization
    msg.cmd = MCP2515_BIT_MODIFY;
    msg.len = 2;

    // Using calculator at https://www.kvaser.com/support/calculators/bit-timing-calculator/
    // Input clock = 8MHz, 500KHz bitrate, SJW = 1, sample point = 75%

    // CNF1: CONFIGURATION REGISTER 1 (ADDRESS: 2Ah)
    // Use default value of 0x00  SJW = 1, BRP = 1 for a Tq of 250nS using a 8MHz oscillator

    // CNF2: CONFIGURATION REGISTER 2 (ADDRESS: 29h)
    msg.addr = CNF2;
    msg.buf[MCP2515_BIT_MASK_BYTE]  = CNF2_BTLMODE_MASK | CNF2_PHSEG1_MASK | CNF2_PRSEG_MASK;
    msg.buf[MCP2515_BIT_DATA_BYTE]  = (1 << CNF2_BTLMODE_SHIFT) | (2 << CNF2_PHSEG1_SHIFT) | (1 << CNF2_PRSET_SHIFT);
    spiIo();

    // CNF3: CONFIGURATION REGISTER 3 (ADDRESS: 28h)
    msg.addr = CNF3;
    msg.buf[MCP2515_BIT_MASK_BYTE] = CNF3_PHSEG2_MASK;
    msg.buf[MCP2515_BIT_DATA_BYTE] = 1 << CNF3_PHSEG2_SHIFT;
    spiIo();


    ///
    /// Receive buffer 0
    ///

    // Mask 0
    // All mask bits set, so all SID bits will be filtered
    msg.cmd = MCP2515_WRITE;
    msg.len = 1;
    msg.addr = RXM0SIDH;
    msg.buf[0] = RXMNSIDH_SID_MASK; // SID<10:3>
    spiIo();
    msg.addr = RXM0SIDL;
    msg.buf[0] = RXMNSIDL_SID_MASK; // SID<2:0>
    spiIo();

    // Filter 0
    // Standard ID,  Extended ID disabled
    msg.addr = RXF0SIDH;
    msg.buf[0] = addr0 >> (8 - RXFNSIDL_SID_SHIFT); // SID<10:3>
    spiIo();
    msg.addr = RXF0SIDL;
    msg.buf[0] = (addr0 << RXFNSIDL_SID_SHIFT); // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    // Filter 1 all F's
    msg.addr = RXF1SIDH;
    msg.buf[0] = addr0 >> (8 - RXFNSIDL_SID_SHIFT); // SID<10:3>
    spiIo();
    msg.addr = RXF1SIDL;
    msg.buf[0] = (addr0 << RXFNSIDL_SID_SHIFT); // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    // Extended Mask 0
    // EID is disabled, so EID fields function as data filters.
    // If any mask bit is set to a zero, that bit will automatically be accepted,
    // regardless of the filter bit.
    msg.addr = RXM0EID8;
    msg.buf[0] = 0;
    spiIo();
    msg.addr = RXM0EID0;
    msg.buf[0] = 0;
    spiIo();

    ///
    /// Receive buffer 1
    ///

    // Mask 1
    // All mask bits set, so all SID bits will be filtered
    msg.cmd = MCP2515_WRITE;
    msg.len = 1;
    msg.addr = RXM1SIDH;
    msg.buf[0] = RXMNSIDH_SID_MASK; // SID<10:3>
    spiIo();
    msg.addr = RXM1SIDL;
    msg.buf[0] = RXMNSIDL_SID_MASK; // SID<2:0>
    spiIo();

    // Filter 2 Standard ID (address 1),  Extended ID disabled
    msg.addr = RXF2SIDH;
    msg.buf[0] = addr1 >> (8 - RXFNSIDL_SID_SHIFT); // SID<10:3>
    spiIo();
    msg.addr = RXF2SIDL;
    msg.buf[0] = (addr1 << RXFNSIDL_SID_SHIFT); // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    // Filter 3, 4, 5 all F's0

    msg.addr = RXF3SIDH;
    msg.buf[0] = RXFNSIDH_SID_MASK; // SID<10:3>
    spiIo();
    msg.addr = RXF3SIDL;
    msg.buf[0] = RXFNSIDL_SID_MASK; // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    msg.addr = RXF4SIDH;
    msg.buf[0] = RXFNSIDH_SID_MASK; // SID<10:3>
    spiIo();
    msg.addr = RXF4SIDL;
    msg.buf[0] = RXFNSIDL_SID_MASK; // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    msg.addr = RXF5SIDH;
    msg.buf[0] = RXFNSIDH_SID_MASK; // SID<10:3>
    spiIo();
    msg.addr = RXF5SIDL;
    msg.buf[0] = RXFNSIDL_SID_MASK; // SID<2:0>, Extended Identifier Enable bit is off
    spiIo();

    // Extended Mask 1
    // EID is disabled, so EID fields function as data filters.
    // If any mask bit is set to a zero, that bit will automatically be accepted,
    // regardless of the filter bit.
    msg.addr = RXM1EID8;
    msg.buf[0] = 0;
    spiIo();
    msg.addr = RXM1EID0;
    msg.buf[0] = 0;
    spiIo();

    // CANCTRL: CAN CONTROL REGISTER (ADDRESS: XFh)
    msg.cmd = MCP2515_BIT_MODIFY;
    msg.len = 2;
    msg.addr = CANCTRL;
    msg.buf[MCP2515_BIT_MASK_BYTE]  = CANCTRL_REQOP_MASK;
    msg.buf[MCP2515_BIT_DATA_BYTE]  = CANCTRL_OPMODE_NORMAL << CANCTRL_REQOP_SHIFT;
    spiIo();
}

unsigned char MCP2515::readCh0(unsigned char *buffer)
{
    unsigned char bytes = 0;
    if (true == ch0Data)
    {
       memcpy(buffer, com.rx0Buf, com.rx0Len);
       bytes = com.rx0Len;
       ch0Data = false;
    }
    return bytes;
}

unsigned char MCP2515::readCh1(unsigned char *buffer)
{
    unsigned char bytes = 0;
    if (true == ch1Data)
    {
       memcpy(buffer, com.rx1Buf, com.rx1Len);
       bytes = com.rx1Len;
       ch1Data = false;
    }
    return bytes;
}

// Non-blocking read of receive buffers 0 and 1
// Returns 0 if no error else contents of ERROR FLAG REGISTER (ADDRESS: 2Dh)
unsigned char MCP2515::poll(void)
{
    int status;
    int i;
    unsigned char rxNResetMask = 0;
    unsigned char eflag = 0;

    status = pollForRxMsg();

    if (MCP2515_RX_FULL == status)
    {
        msg.cmd = MCP2515_RX_STATUS;
        // Quick polling command for filter match and message type
        msg.cmd = MCP2515_RX_STATUS;
        msg.addr = 0;
        spiIo();

        com.rxMsgLoc = (msg.buf[0] & MCP2515_RX_MSG_LOC_MASK) >> MCP2515_RX_MSG_LOC_SHIFT;
        com.rxMsgType = (msg.buf[0] & MCP2515_RX_STATUS_MSG_TYPE_MASK) >> MCP2515_RX_STATUS_MSG_TYPE_SHIFT;

        if (com.rxMsgLoc & RECEIVED_RXB0)
        {
            // Read all RX0 base frame bytes
            msg.cmd = MCP2515_RD_RXB0SIDH;
            msg.addr = MCP2515_ADDR_NULL;
            msg.len = MCP2515_BASE_FRAME_MAX_BYTES;
            spiIo();

            com.rx0Len = msg.buf[MCP2515_BNDLC_BYTE] & TXBNDLC_DLC_MASK;
            for (i = 0; i < com.rx0Len; i++)

            {
                com.rx0Buf[i] = msg.buf[MCP2515_BND0_BYTE + i];
            }

            // Have read RX0 buffer, reset flag
            rxNResetMask |= CANINTF_RX0IF_MASK;

            ch0Data = true;
        }

        if (com.rxMsgLoc & RECEIVED_RXB1)
        {
            // Read all RX1 base frame bytes
            msg.cmd = MCP2515_RD_RXB1SIDH;
            msg.addr = MCP2515_ADDR_NULL;
            msg.len = MCP2515_BASE_FRAME_MAX_BYTES;
            spiIo();

            com.rx1Len = msg.buf[MCP2515_BNDLC_BYTE] & TXBNDLC_DLC_MASK;
            for (i = 0; i < com.rx1Len; i++)
            {
                com.rx1Buf[i] = msg.buf[MCP2515_BND0_BYTE + i];
            }

            // Have read RX1 buffer, reset flag
            rxNResetMask |= CANINTF_RX1IF_MASK;

            ch1Data = true;
        }

        // Clear RX0IF and/or RX1IF bits
        msg.cmd = MCP2515_BIT_MODIFY;
        msg.addr = CANINTF;
        msg.len = 2;
        msg.buf[MCP2515_BIT_MASK_BYTE]  = rxNResetMask;
        msg.buf[MCP2515_BIT_DATA_BYTE]  = 0;
        spiIo();
    }
    else if (MCP2515_GEN_ERROR == status)
    {
        eflag = msg.buf[0];
    }

    return eflag;
}

int MCP2515::pollForRxMsg(void)
{
    int status = MCP2515_RX_EMPTY;

    msg.cmd = MCP2515_READ;
    msg.addr = CANINTF;
    msg.len = 1;
    spiIo();

    // Error condition?
    if (msg.buf[0] & CANINTF_ERRIF_MASK)
    {
        msg.cmd = MCP2515_READ;
        msg.addr = EFLG;
        msg.len = 1;
        // Error in msg.buf[0]
        spiIo();

        status = MCP2515_GEN_ERROR;
    }

    // True if either RX buffer is full
    else if (msg.buf[0] & (CANINTF_RX0IF_MASK | CANINTF_RX1IF_MASK))
    {
        status = MCP2515_RX_FULL;
    }

    return status;
}

void MCP2515::spiReadModifyWrite(void)
{
    unsigned char mask = msg.buf[MCP2515_BIT_MASK_BYTE];
    unsigned char data = msg.buf[MCP2515_BIT_MASK_BYTE];

    msg.cmd = MCP2515_READ;
    msg.len = 1;
    spiIo();

    msg.buf[0] &= ~mask;
    msg.buf[0] |= data;

    msg.cmd = MCP2515_WRITE;
    spiIo();
}

void MCP2515::spiIo(void)
{
    int i;

    SPI.beginTransaction(SPISettings(_spiClk, MSBFIRST, SPI_MODE0));
    digitalWrite(_cs, LOW);

    SPI.transfer(msg.cmd);

    if (MCP2515_ADDR_NULL != msg.addr)
    {
        SPI.transfer(msg.addr);
    }

    for (i = 0; i < msg.len; i++)
    {
        msg.buf[i] = SPI.transfer(msg.buf[i]);
    }

    digitalWrite(_cs, HIGH);
    SPI.endTransaction();
}

unsigned char MCP2515::readRegister(unsigned char addr)
{
    msg.cmd = MCP2515_READ;
    msg.addr = addr;
    msg.len = 1;
    spiIo();

    return msg.buf[0];
}
