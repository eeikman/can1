// Erle Eikman  8/24/2018
#include <Arduino.h>
#include "MCP2515.h"
#include "CanCtrlPanel.h"

CanCtrlPanel::CanCtrlPanel(void)
{
    current = 0;
    currentField = 0;
    lastField = 0;
}

unsigned char CanCtrlPanel::run(void)
{
    unsigned char eflag;

    // Non-blocking read of channels 0 and 1
    eflag = poll();

    // Poll for new character in command buffer
    command();

    //
    return eflag;
};

void CanCtrlPanel::command(void)
{
    if (Serial.available())
    {
        char inChar = (char)Serial.read();

        switch (inChar)
        {
            // CR is end of command
            case '\r':  // Carriage return
                executeCmd();
                Serial.println();
                current = 0;
                currentField = 0;
                lastField = 0;
                break;
            // Allow backspace control on serial terminal
            case 127:   // Delete
            case 8:     // Backspace
                if (current != 0) current--;
                Serial.print('\b');
                Serial.print(' ');
                Serial.print('\b');
                break;
            // Append characters to cmd buffer and
            // echo characters to serial terminal
            default:
                if (CMD_BUFFER > current)
                {
                    // Echo
                    Serial.print(inChar);
                    cmd[current] = inChar;
                    current++;
                }
                break;
        }
    }
}

// Parse the command buffer into fields separated by whitespace
void CanCtrlPanel::nextField(void)
{
    // Advance currentField to next non-whitespace character
    while((currentField < current) && ((cmd[currentField] == ' ') || (cmd[currentField] == '\t')))
    {
        currentField++;
    }

    // Beginning of next field
    lastField = currentField;

    // Find next whitespace in command buffer, or currentField will be end of data in buffer
    while ((currentField < current) && (cmd[currentField] != ' ') && (cmd[currentField] != '\t'))
    {
        currentField++;
    }
}

// Search for string match of command and execute with whitespace delimited parameters
void CanCtrlPanel::executeCmd(void)
{
    int i;
    char *cmdStr;
    nextField();

    for (i = 0; i < COMMANDS;i++)
    {
        if ((0 < current) && (0 == strncmp(commands[i].cmdStr, cmd, currentField - lastField)))
        {
            commands[i].function(this);
            break;
        }
    }
}

void printHello(CanCtrlPanel *canCtrlPanel)
{
    while (canCtrlPanel->currentField < canCtrlPanel->current)
    {
        canCtrlPanel->nextField();
        Serial.println();
        Serial.print("lastField: ");
        Serial.println(canCtrlPanel->lastField, DEC);
        Serial.print("currentField: ");
        Serial.println(canCtrlPanel->currentField, DEC);
    }
}

void reset(CanCtrlPanel *canCtrlPanel)
{
    canCtrlPanel->msg.cmd = MCP2515_RESET;
    canCtrlPanel->spiIo();
    delay(10);
}

void help(CanCtrlPanel *canCtrlPanel)
{
    int i;
    for (i = 0; i < COMMANDS; i++)
    {
        Serial.println();
        Serial.print(commands[i].cmdStr);
        Serial.print(":");
        Serial.print(commands[i].description);
    }
}

void printReg(CanCtrlPanel *canCtrlPanel, unsigned char addr)
{
    unsigned char data;

    data = canCtrlPanel->readRegister(addr);

    Serial.print("0x");
    Serial.print(addr, HEX);
    Serial.print(" 0x");
    Serial.println(data, HEX);
}

void regDump(CanCtrlPanel *canCtrlPanel)
{
    int i;

    for (i = 0; i < MCP2515_REGISTERS; i++)
    {
        printReg(canCtrlPanel, i);
    }
}

