// Erle Eikman  8/24/2018
//
// Running on Arduino Uno clone.
//
// Four bytes representing CPU temperature is sent from
// Linux host.
// The value is converted and displayed on 
// 128x64 OLED on interval of DISPLAY_UPDATE_USEC

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <TimerOne.h>
#include "CanCtrlPanel.h"

#define SPI_CLOCK 1000000
#define SERIAL_CLOCK 115200
#define DISPLAY_UPDATE_USEC 1000000

// MCP2515 SPI chip select pin
#define CS_PIN 10

// This CAN Device IDs
#define THIS_ID1 0x74   // ID for receive buffer 0
#define THIS_ID2 0x75   // ID for receive buffer 1

CanCtrlPanel Canl;

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

bool timeout;

void setup()
{
    Serial.begin(SERIAL_CLOCK);

    Canl.begin(THIS_ID1, THIS_ID2, SPI_CLOCK, CS_PIN);

    // Initialize with the I2C addr 0x3D (for the 128x64)
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.clearDisplay();
    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.display();

    timeout = false;
    Timer1.initialize(DISPLAY_UPDATE_USEC);
    Timer1.attachInterrupt(callback);
}

void loop()
{
    unsigned char bytesRead;
    unsigned char buffer[MCP2515_DLC_MAX];
    long int temperature;

    Canl.run();

    bytesRead = Canl.readCh0(buffer);
    if (0 < bytesRead)
    {
        // Update the display every DISPLAY_UPDATE_USEC
        if (true == timeout)
        {
            display.setCursor(0,0);
            display.clearDisplay();
            display.print("CPU");

            // Space character too wide
            display.setCursor(60,0);

            // Format integer value into xxx.yyy
            temperature = convert(buffer);
            display.print(temperature/1000, DEC);

            // Set '.' closer
            display.setCursor(93,0);
            display.print(".");

            // Only display the tenths value of xxx.yyy
            display.setCursor(110,0);
            display.print((temperature % 1000 / 100), DEC);
            display.display();
            timeout = false;
        }
    }
    bytesRead = Canl.readCh1(buffer);
}

void callback()
{
    timeout = true;
}

long int convert(unsigned char *buffer)
{
    long int value;

    // Buffer from CAN is big endian
    value = buffer[0];
    value <<= 8;
    value |= buffer[1];
    value <<= 8;
    value |= buffer[2];
    value <<= 8;
    value |= buffer[3];

    return value;
}

void dataDump(unsigned char *data, int length)
{
    int i;
    unsigned char *current = data;

    for(i = 0; i < length; i++)
    {
        Serial.println(*current, HEX);
        current++;
    }
}

