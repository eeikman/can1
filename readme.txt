Developed to run on Ardinuo Uno clone

This was an exercise in creating an interface written in C++ for MCP2515  CAN chip.

Four bytes representing CPU temperature is sent from a Rasberry Pi Linux host over a CAN bus. 
The value is converted and displayed on a SSD1306 128x64 OLED

Here is a link that was alive 10/03/18 that gives details on interfacing a MCP2515 module to the Rasberry Pi

https://vimtut0r.com/2017/01/17/can-bus-with-raspberry-pi-howtoquickstart-mcp2515-kernel-4-4-x/
