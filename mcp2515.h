// Erle Eikman  8/24/2018
// MCP2515.h

#ifndef MCP2515_H
#define MCP2515_H

///
/// MCP2515 Control, Configuration and Status Registers
///

// CNF1: CONFIGURATION REGISTER 1 (ADDRESS: 2Ah)
// SPI Bit Modify Command allowed
typedef struct
{
    unsigned char brp       : 5;      // Baud rate prescaler
    unsigned char sjw       : 2;      // Synchronization jump width
} _CNF1;

#define CNF1                    0x2A
    #define CNF1_BRP_MASK       0x3F
    #define CNF1_BRP_SHIFT      0
    #define CNF1_SJW_MASK       0xC0
    #define CNF1_SJW_SHIFT      6


// CNF2: CONFIGURATION REGISTER 2 (ADDRESS: 29h)
// SPI Bit Modify Command allowed
typedef struct
{
    unsigned char prseg     : 3;    // Propagation segment length
    unsigned char phseg1    : 3;    // Phase 1 length
    unsigned char sam       : 1;    // Sample point configuraton
    unsigned char btlmode   : 1;    // Phase 2 length config enable
} _CNF2;

#define CNF2                    0x29
    #define CNF2_PRSEG_MASK     0x07
    #define CNF2_PRSET_SHIFT    0
    #define CNF2_PHSEG1_MASK    0x38
    #define CNF2_PHSEG1_SHIFT   3
    #define CNF2_SAM_MASK       0x40
    #define CNF2_SAM_SHIFT      6
    #define CNF2_BTLMODE_MASK   0x80
    #define CNF2_BTLMODE_SHIFT  7

// CNF3: CONFIGURATION REGISTER 3 (ADDRESS: 28h)
// SPI Bit Modify Command allowed
typedef struct
{
    unsigned char phseg2    : 3;    // Phase 2 length
    unsigned char reserved  : 3;
    unsigned char wakfil    : 1;    // Wake-up filer enable
    unsigned char sof       : 1;    // SOF pin function
} _CNF3;

#define CNF3                    0x28
    #define CNF3_PHSEG2_MASK    0x07
    #define CNF3_PHSEG2_SHIFT   0
    #define CNF3_WAKFIL_MASK    0x40
    #define CNF3_WAKFIL_SHIFT   6
    #define CNF3_SOF_MASK       0x80
    #define CNF3_SOF_SHIFT      7

// CANINTE: CAN INTERRUPT ENABLE REGISTER (ADDRESS: 2Bh)
// SPI Bit Modify Command allowed and recommended
typedef struct
{
    unsigned char rx0ie     : 1;    // Receive Buffer 0 Full Interrupt Enable
    unsigned char rx1ie     : 1;    // Receive Buffer 1 Full Interrupt Enable
    unsigned char tx0ie     : 1;    // Transmit Buffer 0 Empty Interrupt Enable
    unsigned char tx1ie     : 1;    // Transmit Buffer 1 Empty Interrupt Enable
    unsigned char tx2ie     : 1;    // Transmit Buffer 2 Empty Interrupt Enable
    unsigned char errie     : 1;    // Error Interrupt Enable (EFLG register)
    unsigned char wakie     : 1;    // Wake-up Interrupt Enable
    unsigned char merre     : 1;    // Message Error Interrupt Enable
} _CANINTE;

#define CANINTE                 0x2B
    #define CANINTE_RX0IE_MASK  0x01
    #define CANINTE_RX0IE_SHIFT 0
    #define CANINTE_RX1IE_MASK  0x02
    #define CANINTE_RX1IE_SHIFT 1
    #define CANINTE_TX0IE_MASK  0x04
    #define CANINTE_TX0IE_SHIFT 2
    #define CANINTE_TX1IE_MASK  0x08
    #define CANINTE_TX1IE_SHIFT 3
    #define CANINTE_TX2IE_MASK  0x10
    #define CANINTE_TX2IE_SHIFT 4
    #define CANINTE_ERRIE_MASK  0x20
    #define CANINTE_ERRIE_SHIFT 5
    #define CANINTE_WAKIE_MASK  0x40
    #define CANINTE_WAKIE_SHIFT 6
    #define CANINTE_MERRE_MASK  0x80
    #define CANINTE_MERRE_SHIFT 7


// CANINTF: CAN INTERRUPT FLAG REGISTER (ADDRESS: 2Ch)
// SPI Bit Modify Command allowed and recommended
typedef struct
{
    unsigned char rx0if     : 1;    // Receive Buffer 0 Full Interrupt
    unsigned char rx1if     : 1;    // Receive Buffer 1 Full Interrupt
    unsigned char tx0if     : 1;    // Transmit Buffer 0 Empty Interrupt
    unsigned char tx1if     : 1;    // Transmit Buffer 1 Empty Interrupt
    unsigned char tx2if     : 1;    // Transmit Buffer 2 Empty Interrupt
    unsigned char errif     : 1;    // Error Interrupt (EFLG register)
    unsigned char wakif     : 1;    // Wake-up Interrupt
    unsigned char merrf     : 1;    // Message Error Interrupt
} _CANINTF;

#define CANINTF                 0x2C
    #define CANINTF_RX0IF_MASK  0x01
    #define CANINTF_RX0IF_SHIFT 0
    #define CANINTF_RX1IF_MASK  0x02
    #define CANINTF_RX1IF_SHIFT 1
    #define CANINTF_TX0IF_MASK  0x04
    #define CANINTF_TX0IF_SHIFT 2
    #define CANINTF_TX1IF_MASK  0x08
    #define CANINTF_TX1IF_SHIFT 3
    #define CANINTF_TX2IF_MASK  0x10
    #define CANINTF_TX2IF_SHIFT 4
    #define CANINTF_ERRIF_MASK  0x20
    #define CANINTF_ERRIF_SHIFT 5
    #define CANINTF_WAKIF_MASK  0x40
    #define CANINTF_WAKIF_SHIFT 6
    #define CANINTF_MERRF_MASK  0x80
    #define CANINTF_MERRF_SHIFT 7

// EFLG: ERROR FLAG REGISTER (ADDRESS: 2Dh)
// SPI Bit Modify Command allowed
typedef struct
{
    unsigned char ewarn     : 1;    // Error Warning Flag
    unsigned char rxwar     : 1;    // Receive Error Warning Flag
    unsigned char txwar     : 1;    // Transmit Error Warning Flag
    unsigned char rxep      : 1;    // Receive Error-Passive Flag
    unsigned char txep      : 1;    // Transmit Error-Passive Flag
    unsigned char txb0      : 1;    // Bus-Off Error Flag
    unsigned char rx0ovr    : 1;    // Receive Buffer 0 Overflow Flag
    unsigned char rx1ovr    : 1;    // Receive Buffer 1 Overflow Flag
} _EFLG;

#define EFLG                 0x2D
#define EFLG_EWARN_MASK     0x01
#define EFLG_EWARN_SHIFT    0
#define EFLG_RXWAR_MASK     0x02
#define EFLG_RXWAR_SHIFT    1
#define EFLG_TXWAR_MASK     0x04
#define EFLG_TXWAR_SHIFT    2
#define EFLG_RXEP_MASK      0x08
#define EFLG_RXEP_SHIFT     3
#define EFLG_TXEP_MASK      0x10
#define EFLG_TXEP_SHIFT     4
#define EFLG_TXBO_MASK      0x20
#define EFLG_TXBO_SHIFT     5
#define EFLG_RX0OVR_MASK    0x40
#define EFLG_RX0OVR_SHIFT   6
#define EFLG_RX1OVR_MASK    0x80
#define EFLG_RX1OVR_SHIFT   7

// CANCTRL: CAN CONTROL REGISTER (ADDRESS: XFh)
// SPI Bit Modify Command allowed
typedef struct
{
    unsigned char clkpre    : 2;    // CLKOUT pin prescaler
    unsigned char clken     : 1;    // CLKOUT pin enable
    unsigned char osm       : 1;    // One-shot mode
    unsigned char abat      : 1;    // Abort all pending transmissions
    unsigned char reqop     : 3;    // Request operation mode see pg 60
} _CANCTRL;
#define CANCTRL                     0x0F
    #define CANCTRL_CLKPRE_MASK     0x03
    #define CANCTRL_CLKPRE_SHIFT    0
    #define CANCTRL_CLKEN_MASK      0x04
    #define CANCTRL_CLKEN_SHIFT     2
    #define CANCTRL_OSM_MASK        0x08
    #define CANCTRL_OSM_SHIFT       3
    #define CANCTRL_ABAT_MASK       0x10
    #define CANCTRL_ABAT_SHIFT      4
    #define CANCTRL_REQOP_MASK      0xE0
    #define CANCTRL_REQOP_SHIFT     5

    #define CANCTRL_OPMODE_NORMAL       0
    #define CANCTRL_OPMODE_SLEEP        1
    #define CANCTRL_OPMODE_LOOPBACK     2
    #define CANCTRL_OPMODE_LISTEN_ONLY  3
    #define CANCTRL_OPMODE_CONFIG       4

// TXRTSCTRL: TXnRTS PIN CONTROL AND STATUS REGISTER (ADDRESS: 0Dh)
// These pins will be unused, ignore

// CANSTAT: CAN STATUS REGISTER (ADDRESS: XEh)
typedef struct
{
    unsigned char rsvd1     : 1;
    unsigned char icod      : 3;    // Interrupt flag code bits
    unsigned char rsvd2     : 1;
    unsigned char opmode    : 3;    // Operation mode see pg 61
} _CANSTAT;
#define CANSTAT                     0x0E
    #define CANSTAT_ICOD_MASK       0x0E
    #define CANSTAT_ICOD_SHIFT      1
    #define CANSTAT_OPMODE_MASK     0x0E0
    #define CANSTAT_OPMODE_SHIFT    5

    #define CANSTAT_ICOD_NONE           0
    #define CANSTAT_ICOD_ERROR          1
    #define CANSTAT_ICOD_WAKE           2
    #define CANSTAT_ICOD_TXB0           3
    #define CANSTAT_ICOD_TXB1           4
    #define CANSTAT_ICOD_TXB2           5
    #define CANSTAT_ICOD_RXB0           6
    #define CANSTAT_ICOD_RXB1           7

    #define CANSTAT_OPMODE_NORMAL       0
    #define CANSTAT_OPMODE_SLEEP        1
    #define CANSTAT_OPMODE_LOOPBACK     2
    #define CANSTAT_OPMODE_LISTEN_ONLY  3
    #define CANSTAT_OPMODE_CONFIG       4

// RXFnSIDH: FILTER n STANDARD IDENTIFIER REGISTER HIGH
// (ADDRESS: 00h, 04h, 08h, 10h, 14h, 18h)
typedef struct
{
    unsigned char sid   : 8; // SID<10:3>: Standard Identifier Filter
} RXFNSIDH;
#define RXF0SIDH 0x00
#define RXF1SIDH 0x04
#define RXF2SIDH 0x08
#define RXF3SIDH 0x10
#define RXF4SIDH 0x14
#define RXF5SIDH 0x18
    #define RXFNSIDH_SID_MASK   0xFF

// RXFnSIDL: FILTER n STANDARD IDENTIFIER REGISTER LOW
// (ADDRESS: 01h, 05h, 09h, 11h, 15h, 19h)
typedef struct
{
    unsigned char eid   : 2; // EID<17:16>: Extended Identifier Filter
    unsigned char rsvd1 : 1;
    unsigned char exid  : 1; // Extended Identifier Enable
    unsigned char rsvd2 : 1;
    unsigned char sid   : 3; // SID<2:0>: Standard Identifier Filter
} RXFNSIDL;
#define RXF0SIDL 0x01
#define RXF1SIDL 0x05
#define RXF2SIDL 0x09
#define RXF3SIDL 0x11
#define RXF4SIDL 0x15
#define RXF5SIDL 0x19
    #define RXFNSIDL_EID_MASK   0x03
    #define RXFNSIDL_EID_SHIFT  0
    #define RXFNSIDL_EXID_MASK  0x08
    #define RXFNSIDL_EXID_SHIFT 3
    #define RXFNSIDL_SID_MASK   0xE0
    #define RXFNSIDL_SID_SHIFT  5

// RXMnSIDH: MASK n STANDARD IDENTIFIER REGISTER HIGH
// (ADDRESS: 20h, 24h)
typedef struct
{
    unsigned char sid   : 8; // SID<10:3>: Standard Identifier Mask
} RXMNSIDH;
#define RXM0SIDH 0x20
#define RXM1SIDH 0x24
    #define RXMNSIDH_SID_MASK   0xFF

// RXMnSIDL: MASK n STANDARD IDENTIFIER REGISTER LOW
// (ADDRESS: 21h, 25h)
typedef struct
{
    unsigned char eid   : 2; // EID<17:16>: Extended Identifier Mask
    unsigned char rsvd1 : 3;
    unsigned char sid   : 3; // SID<2:0>: Standard Identifier Mask
} RXMNSIDL;
#define RXM0SIDL 0x21
#define RXM1SIDL 0x25
    #define RXMNSIDL_EID_MASK   0x03
    #define RXMNSIDL_EID_SHIFT  0
    #define RXMNSIDL_SID_MASK   0xE0
    #define RXMNSIDL_SID_SHIFT  5

// RXFnEID8: FILTER n EXTENDED IDENTIFIER REGISTER HIGH
// (ADDRESS: 02h, 06h, 0Ah, 12h, 16h, 1Ah
typedef struct
{
    unsigned char eid   : 8; // EID<15:8>: Extended Identifier, data filter if EID is disabled
} RXFNEID8;
#define RXF0EID8 0x02
#define RXF1EID8 0x06
#define RXF2EID8 0x0A
#define RXF3EID8 0x12
#define RXF4EID8 0x16
#define RXF5EID8 0x1A
    #define RXFNEID8_EID_MASK 0xFF

// RXFnEID0: FILTER n EXTENDED 1 REGISTER LOW
// (ADDRESS: 03h, 07h, 0Bh, 13h, 17h, 1Bh)
typedef struct
{
    unsigned char eid   : 8; // EID<7:0>: Extended Identifier
} RXFNEID0;
#define RXF0EID0 0x03
#define RXF1EID0 0x07
#define RXF2EID0 0x0B
#define RXF3EID0 0x13
#define RXF4EID0 0x17
#define RXF5EID0 0x1B
    #define RXFNEID0_EID_MASK 0xFF

// RXMnEID8: MASK n EXTENDED IDENTIFIER REGISTER HIGH
// (ADDRESS: 22h, 26h)
typedef struct
{
    unsigned char eid   : 8; // EID<15:8>: Extended Identifier Mask
} RXMNEID8;
#define RXM0EID8 0x22
#define RXM1EID8 0x26

// RXMnEID0: MASK n EXTENDED IDENTIFIER REGISTER LOW
// (ADDRESS: 23h, 27h)
typedef struct
{
    unsigned char eid   : 8; // EID<7:0>: Extended Identifier Mask
} RXMNEID0;
#define RXM0EID0 0x23
#define RXM1EID0 0x27


// TXBnCTRL: TRANSMIT BUFFER n CONTROL REGISTER (ADDRESS: 30h, 40h, 50h)
typedef struct
{
    unsigned char txp       : 2;    // Transmit buffer priority 0 is lowest
    unsigned char rsvd1     : 1;
    unsigned char txreq     : 1;    // Message transmit request
    unsigned char txerr     : 1;    // Message transmit error
    unsigned char mloa      : 1;    // Message lost arbitration
    unsigned char abtf      : 1;    // Message aborted
    unsigned char rsvd2     : 1;
} TXBNCTRL;
#define TXB0CTRL 0x30
#define TXB1CTRL 0x40
#define TXB2CTRL 0x50
    #define TXBNCTRL_TXP_MASK       0x03
    #define TXBNCTRL_TXP_SHIFT      0
    #define TXBNCTRL_TXREQ_MASK     0x08
    #define TXBNCTRL_TXREQ_SHIFT    3
    #define TXBNCTRL_TXERR_MASK     0x10
    #define TXBNCTRL_TXERR_SHIFT    4
    #define TXBNCTRL_MLOA_MASK      0x20
    #define TXBNCTRL_MLOA_SHIFT     5
    #define TXBNCTRL_ABTF_MASK      0x40
    #define TXBNCTRL_ABTF_SHIFT     6

// TXBnSIDH: TRANSMIT BUFFER n STANDARD IDENTIFIER REGISTER HIGH
// (ADDRESS: 31h, 41h, 51h)
typedef struct
{
    unsigned char sid       : 8;    // SID<10:3>: Standard Identifier bits
} TXBNSIDH;
#define TXB0SIDH    0x31
#define TXB1SIDH    0x41
#define TXB2SIDH    0x51
    #define TXBNSIDH_SID_MASK 0xFF

// TXBnSIDL: TRANSMIT BUFFER n STANDARD IDENTIFIER REGISTER LOW
// (ADDRESS: 32h, 42h, 52h)
typedef struct
{
    unsigned char eid       : 2;    // EID<17:16>: Extended Identifier bits
    unsigned char rsvd1     : 1;
    unsigned char exide     : 1;    // Extended Identifier Enable bit
    unsigned char rsvd2     : 1;
    unsigned char sid       : 3;    // SID<2:0>: Standard Identifier bits
} TXBNSIDL;
#define TXB0SIDL    0x32
#define TXB1SIDL    0x42
#define TXB2SIDL    0x52
    #define TXBNSIDL_EID_MASK       0x03
    #define TXBNSIDL_EID_SHIFT      0
    #define TXBNSIDL_EXIDE_MASK     0x08
    #define TXBNSIDL_EXIDE_SHIFT    3
    #define TXBNSIDL_SID_MASK       0xE0
    #define TXBNSIDL_SID_SHIFT      5

// TXBnEID8: TRANSMIT BUFFER n EXTENDED IDENTIFIER 8 REGISTER HIGH
// (ADDRESS: 33h, 43h, 53h)
typedef struct
{
    unsigned char eid   : 8;
} TXBNEID8;
#define TXB0EID8 0x33
#define TXB1EID8 0x43
#define TXB2EID8 0x53
    #define TXBNEID8_EID_MASK 0xFF

// TXBnEID0: TRANSMIT BUFFER n EXTENDED IDENTIFIER 0 REGISTER LOW
// (ADDRESS: 34h, 44h, 54h)
typedef struct
{
    unsigned char eid   : 8;
} TXBNEID0;
#define TXB0EID0 0x34
#define TXB1EID0 0x44
#define TXB1EID0 0x54
    #define TXBNEID0_EID_MASK 0xFF

// TXBnDLC: TRANSMIT BUFFER n DATA LENGTH CODE REGISTER
// (ADDRESS: 35h, 45h, 55h)
typedef struct
{
    unsigned char dlc       : 4;    // DLC<3:0>: Data Length Code bits (max 8)
    unsigned char rsvd1     : 2;
    unsigned char rtr       : 1;    // Remote Transmission Request
    unsigned char rsvd2     : 1;
} TXBNDLC;
#define TXB0DLC     0x35
#define TXB1DLC     0x45
#define TXB2DLC     0x55
    #define TXBNDLC_DLC_MASK    0x0F
    #define TXBNDLC_DLC_SHIFT   0
    #define TXBNDLC_RTR_MASK    0x40
    #define TXBNDLC_RTR_SHIFT   6

// RXB0CTRL: RECEIVE BUFFER 0 CONTROL REGISTER (ADDRESS: 60h)
typedef struct
{
    unsigned char filhit0   : 1;    // Filter Hit
    unsigned char bukt1     : 1;    // Read-only copy of BUKT bit
    unsigned char bukt      : 1;    // Rollover enable
    unsigned char rxrtr     : 1;    // Received remote transfer request
    unsigned char rsvd1     : 1;
    unsigned char rxm       : 2;    // RXM<1:0>: Receive Buffer Operating mode 3 = filter off, 0 = filter on
     unsigned char rsvd2     : 1;
} _RXB0CTRL;
#define RXB0CTRL 0x60
    #define RXB0CTRL_FILHIT0_MASK   0x01
    #define RXB0CTRL_FILHIT0_SHIFT  0
    #define RXB0CTRL_BUKT1_MASK     0x02
    #define RXB0CTRL_BUKT1_SHIFT    1
    #define RXB0CTRL_BUKT_MASK      0x04
    #define RXB0CTRL_BUKT_SHIFT     2
    #define RXB0CTRL_RXRTR_MASK     0x08
    #define RXB0CTRL_RXRTR_SHIFT    3
    #define RXB0CTRL_RXM_MASK       0x60
    #define RXB0CTRL_RXM_SHIFT      5
        #define RXB0CTRL_RXM_ON     0   // Mask/filters applied as per documentation
        #define RXB0CTRL_RXM_OFF    3   // Turns mask/filters off; receives any message

// RXB0CTRL: RECEIVE BUFFER 0 CONTROL REGISTER (ADDRESS: 60h)
typedef struct
{
    unsigned char filhit    : 3;    // FILHIT<2:0>: Filter Hit see MCP2515 specification sheet
    unsigned char rxrtr     : 1;    // Received remote transfer request
    unsigned char rsvd1     : 1;
    unsigned char rxm       : 2;    // RXM<1:0>: Receive Buffer Operating mode 3 = filter off, 0 = filter on
    unsigned char rsvd2     : 1;
} _RXB1CTRL;
#define RXB1CTRL 0x70
    #define RXB1CTRL_FILHIT_MASK    0x07
    #define RXB1CTRL_FILHIT_SHIFT   0
    #define RXB1CTRL_RXRTR_MASK     0x08
    #define RXB1CTRL_RXRTR_SHIFT    3
    #define RXB1CTRL_RXM_MASK       0x60
    #define RXB1CTRL_RXM_SHIFT      5

// Transmit Buffer base addresses
#define TXB0D0 0x36
#define TXB1D0 0x46
#define TXB2D0 0x56

// Receive Buffer base addresses
#define RXB0D0 0x66
#define RXB01D0 0x76


///
/// SPI Commands
///

#define MCP2515_RESET               0xC0 // Resets internal registers to the default state

#define MCP2515_READ                0x03 // Reads data from the register beginning at selected address.
#define MCP2515_WRITE               0x02 // Writes data to the register beginning at the selected address.

#define MCP2515_RD_RXB				0x90
    #define MCP2515_RD_RXB0SIDH		0x90 // Read RX Buffer 0, Start at RXB0SIDH
    #define MCP2515_RD_RXB0D0		0x92 // Read RX Buffer 0, Start at RXB0D0
    #define MCP2515_RD_RXB1SIDH		0x94 // Read RX Buffer 1, Start at RXB1SIDH
    #define MCP2515_RD_RXB1D0		0x96 // Read RX Buffer 1, Start at RXB1D0

#define MCP2515_LD_TXB              0x40
    #define MCP2515_LD_TXB0SIDH     0x40 // Loads TX Buffer 0, Start at TXB0SIDH
    #define MCP2515_LD_TXB0D0       0x41 // Loads TX Buffer 0, Start at TXB0D0
    #define MCP2515_LD_TXB1SIDH     0x42 // Loads TX Buffer 1, Start at TXB1SIDH
    #define MCP2515_LD_TXB1D0       0x43 // Loads TX Buffer 1, Start at TXB1D0
    #define MCP2515_LD_TXB2SIDH     0x44 // Loads TX Buffer 2, Start at TXB2SIDH
    #define MCP2515_LD_TXB2D0       0x45 // Loads TX Buffer 2, Start at TXB2D0

#define MCP2515_RTS_TXB             0x80
    #define MCP2515_RTS_TXBUF0      0x80 // Begin message transmission sequence TXBUF0
    #define MCP2515_RTS_TXBUF1      0x81 // Begin message transmission sequence TXBUF1
    #define MCP2515_RTS_TXBUF2      0x82 // Begin message transmission sequence TXBUF2

#define MCP2515_RD_STATUS           0xA0 // Quick polling command for TX and RX functions
// Data out fields from MCP2515_RD_STATUS
#define MCP2515_RD_STATUS_RX0IF		0x01 // (CANINTF<0>) Receive Buffer 0 Full Interrupt
#define MCP2515_RD_STATUS_RX1IF		0x02 // (CANINTF<1>) Receive Buffer 1 Full Interrupt
#define MCP2515_RD_STATUS_TX0REQ	0x04 // (TXB0CTRL<3>) TXB0 Message transmit request
#define MCP2515_RD_STATUS_TX0IF		0x08 // (CANINTF<2>) Transmit Buffer 0 Empty Interrupt
#define MCP2515_RD_STATUS_TX1REQ	0x10 // (TXB1CTRL<3>) TXB1 Message transmit request
#define MCP2515_RD_STATUS_TX1IF		0x20 // (CANINTF<3>) Transmit Buffer 1 Empty Interrupt
#define MCP2515_RD_STATUS_TX2REQ	0x40 // (TXB2CTRL<3>) TXB2 Message transmit request
#define MCP2515_RD_STATUS_TX2IF		0x80 // (CANINTF<4>) Transmit Buffer 2 Empty Interrupt

#define MCP2515_RX_STATUS           0xB0 // Quick polling command for filter match and message type
// Data out fields from MCP2515_RD_STATUS
#define MCP2515_RX_STATUS_FILTER_MASK   0x07
#define MCP2515_RX_STATUS_FILTER_SHIFT  0
    // Field values
    typedef enum {
        RXFO = 0x00,
        RXF1,
        RXF2,
        RXF3,
        RXF4,
        RXF5,
        RXF0_ROLLOVER,  // (rollover to RXB1)
        RXF1_ROLLOVER   // (rollover to RXB1)
    } MCP2515_RX_STATUS_FILTER_MATCH;

#define MCP2515_RX_MSG_LOC_MASK   0xC0
#define MCP2515_RX_MSG_LOC_SHIFT  6
    // Field values
    typedef enum {
        RECEIVED_NONE = 0x00,   // No RX message
        RECEIVED_RXB0,          // Message in RXB0
        RECEIVED_RXB1,          // Message in RXB1
        RECEIVED_BOTH           // Messages in both buffers
    } MCP2515_RX_MSG_LOC;

#define MCP2515_RX_STATUS_MSG_TYPE_MASK   0x18
#define MCP2515_RX_STATUS_MSG_TYPE_SHIFT  3
    // Field values
    typedef enum {
        STD_DATA_FRAME = 0x00,  // Standard data frame
        STD_REMOTE_FRAME,       // Standard remote frame
        EXT_DATA_FRAME,         // Extended data frame
        EXT_REMOTE_FRAME        // Extended remote frame
    } MCP2515_RX_STATUS_MSG_TYPE;

#define MCP2515_BIT_MODIFY          0x05 // Bit control of selected registers
    // Message buffer byte position
    #define MCP2515_BIT_MASK_BYTE 0
    #define MCP2515_BIT_DATA_BYTE 1

// CAN standard of max Data Length Code
#define MCP2515_DLC_MAX  8

// Arbitration, Control and Data bytes of a CAN base frame
#define MCP2515_BASE_FRAME_MAX_BYTES 13

// Out-of-bounds address here indicates message has no address field
#define MCP2515_ADDR_NULL 0xFF

// Address range of MCP2515 register set
#define MCP2515_REGISTERS 0x80

typedef struct
{
    unsigned char cmd;  // MCP2515 SPI command
    unsigned char addr; // Register address (base address, successive SPI r/w)
    unsigned char len;  // Length of data in TX buf
    unsigned char buf[MCP2515_BASE_FRAME_MAX_BYTES];
} Mcp2515SpiIoMsg;

typedef struct
{

    unsigned char rx0Len;  // Length of data in RX buf 0
    unsigned char rx1Len;  // Length of data in RX buf 1
    unsigned char rx0Buf[MCP2515_DLC_MAX];
    unsigned char rx1Buf[MCP2515_DLC_MAX];
    MCP2515_RX_MSG_LOC rxMsgLoc;           // Received Message location
    MCP2515_RX_STATUS_MSG_TYPE rxMsgType;   //  Message type
} Mcp2515Com;

// CAN Base Frame byte positions (Receive and transmit)
#define MCP2515_BNSIDH_BYTE 0
#define MCP2515_BNSIDL_BYTE 1
#define MCP2515_BNEID8_BYTE 2
#define MCP2515_BNEID0_BYTE 3
#define MCP2515_BNDLC_BYTE  4
#define MCP2515_BND0_BYTE   5
#define MCP2515_BND1_BYTE   6
#define MCP2515_BND2_BYTE   7
#define MCP2515_BND3_BYTE   8
#define MCP2515_BND4_BYTE   9
#define MCP2515_BND5_BYTE   10
#define MCP2515_BND6_BYTE   11
#define MCP2515_BND7_BYTE   12

#define MCP2515_RX_EMPTY    0
#define MCP2515_RX_FULL     1
#define MCP2515_GEN_ERROR   2

// Class Definitions
class MCP2515
{
public:
    void begin(int addr0, int addr1, int spiClk, int cs);
    unsigned char poll(void);

    unsigned char readCh0(unsigned char* buffer);
    unsigned char readCh1(unsigned char* buffer);

    // Refer to MCP2515 register set
    unsigned char readRegister(unsigned char addr);

protected:
    Mcp2515SpiIoMsg msg;
    void spiIo(void);

private:
    int _addr0; // Address for receive register 0
    int _addr1; // Address for receive register 1
    int _spiClk; // SPI clock frequency
    int _cs;    // SPI chip select pin

    bool ch0Data;
    bool ch1Data;

    Mcp2515Com com;

    int pollForRxMsg(void);
    void spiReadModifyWrite();
};

#endif
